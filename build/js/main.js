// // -----------------more------------
// $(document).ready(function () {
//     $(".show-more a").each(function() {
//         var $link = $(this);
//         var $content = $link.parent().prev("div.text-content");
//
//         console.log($link);
//
//         var visibleHeight = $content[0].clientHeight;
//         var actualHide = $content[0].scrollHeight - 1; // -1 is needed in this example or you get a 1-line offset.
//
//         console.log(actualHide);
//         console.log(visibleHeight);
//
//         if (actualHide > visibleHeight) {
//             $link.show();
//         } else {
//             $link.hide();
//         }
//     });
//
// });

$(document).on("click", ".service", function () {
    $(this).toggleClass('hover');
});

$('.form-order').submit(function (e) {
    e.preventDefault();

    var data = $('.form-order').serializeArray();
    $.ajax({
        // type: "post",
        // url: "index.php",
        type: "get",
        url: "index.html",
        data: data
    }).done(function () {
        $(this).find("input").val("");
        $('.modal-content').children().hide();
        setTimeout (function () {
            $('.thanks').show();
        }, 300);
        setTimeout (function () {
            $('.thanks').fadeOut();
        }, 4000);
        $("form").trigger("reset");
        setTimeout (function () {
            $('.modal-content').children().show();
            $('.thanks').css('display', 'none');
        }, 5000);
    });
    return false;
});


$(document).ready(function() {
    $('.but').click(function(event) {
        $('.service_desc').children().hide();
        const num = $(this).attr('data-num');
        $('#s'+num).show();
    });
});

$(function(){
    var url = window.location.pathname;
    $('.nav-link').each(function () {
        var link = $(this).attr('href');
        if(url == link) {
            $(this).addClass('active');
        }
        if(url === '/sale.html' || url === '/install.html') {
            $('.all').addClass('active');
        }
    });
});


$(document).ready(function() {
    $('.repair').click(function(event) {
            $('.more').show();
            $('#msg').hide();
    });
});

$(document).ready(function() {
    $('.pics').click(function(event) {
        $('.seller').toggleClass('seller-grid');
        $('.seller_lg').toggleClass('seller-inner-grid');
        $('.seller-inner-content').toggleClass(' seller-inner-grid');
        $('.seller-desc').hide();
        $('.seller-button').addClass('seller-button-grid');
        $('.button-desc').show();
        $('.grid').removeClass('col-12').addClass('col-lg-3 col-md-6 col-xs-12');
    });
    $('.rows').click(function(event) {
        $('.grid').removeClass('col-lg-3 col-md-6 col-xs-12').addClass('col-12');
        $('.seller-desc').show();
        $('.seller').removeClass('seller-grid');
        $('.seller_lg').removeClass('seller-inner-grid');
        $('.seller-inner-content').removeClass(' seller-inner-grid');
        $('.seller-button').removeClass('seller-button-grid');
        $('.button-desc').hide();
    });

    $('.button-desc').click(function (event) {
        let desc = this.siblings($('.seller-desc'));
        desc.css('display', 'block');
        desc.append($('.modal-body-desc'))

    })
});









const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./brands.db', (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Connected to the brands database.');
});

let brands = [];
let sql = `SELECT * FROM BRANDS WHERE id = ?`;
let id = 4;

db.get(sql, [id], (err, row) => {
    if (err) {
        return console.error(err.message);
    }
    return row
});

console.log(row.id);

// db.all(sql, [], (err, rows) => {
//     if (err) {
//         throw err;
//     } else {
//         setValue(rows);
//     }
// });

// function setValue(value) {
//      brands = value;
//      console.log(brands);
// }






// const getRow = function(a) {
//     db.serialize(() => {
//         db.each(`SELECT * FROM brands WHERE id = a`, (err, row) => {
//             if (err) {
//                 console.error(err.message);
//             }
//             return row;
//             // console.log(row);
//         });
//     });
// };
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIC8vIC0tLS0tLS0tLS0tLS0tLS0tbW9yZS0tLS0tLS0tLS0tLVxuLy8gJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuLy8gICAgICQoXCIuc2hvdy1tb3JlIGFcIikuZWFjaChmdW5jdGlvbigpIHtcbi8vICAgICAgICAgdmFyICRsaW5rID0gJCh0aGlzKTtcbi8vICAgICAgICAgdmFyICRjb250ZW50ID0gJGxpbmsucGFyZW50KCkucHJldihcImRpdi50ZXh0LWNvbnRlbnRcIik7XG4vL1xuLy8gICAgICAgICBjb25zb2xlLmxvZygkbGluayk7XG4vL1xuLy8gICAgICAgICB2YXIgdmlzaWJsZUhlaWdodCA9ICRjb250ZW50WzBdLmNsaWVudEhlaWdodDtcbi8vICAgICAgICAgdmFyIGFjdHVhbEhpZGUgPSAkY29udGVudFswXS5zY3JvbGxIZWlnaHQgLSAxOyAvLyAtMSBpcyBuZWVkZWQgaW4gdGhpcyBleGFtcGxlIG9yIHlvdSBnZXQgYSAxLWxpbmUgb2Zmc2V0LlxuLy9cbi8vICAgICAgICAgY29uc29sZS5sb2coYWN0dWFsSGlkZSk7XG4vLyAgICAgICAgIGNvbnNvbGUubG9nKHZpc2libGVIZWlnaHQpO1xuLy9cbi8vICAgICAgICAgaWYgKGFjdHVhbEhpZGUgPiB2aXNpYmxlSGVpZ2h0KSB7XG4vLyAgICAgICAgICAgICAkbGluay5zaG93KCk7XG4vLyAgICAgICAgIH0gZWxzZSB7XG4vLyAgICAgICAgICAgICAkbGluay5oaWRlKCk7XG4vLyAgICAgICAgIH1cbi8vICAgICB9KTtcbi8vXG4vLyB9KTtcblxuJChkb2N1bWVudCkub24oXCJjbGlja1wiLCBcIi5zZXJ2aWNlXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdob3ZlcicpO1xufSk7XG5cbiQoJy5mb3JtLW9yZGVyJykuc3VibWl0KGZ1bmN0aW9uIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgdmFyIGRhdGEgPSAkKCcuZm9ybS1vcmRlcicpLnNlcmlhbGl6ZUFycmF5KCk7XG4gICAgJC5hamF4KHtcbiAgICAgICAgLy8gdHlwZTogXCJwb3N0XCIsXG4gICAgICAgIC8vIHVybDogXCJpbmRleC5waHBcIixcbiAgICAgICAgdHlwZTogXCJnZXRcIixcbiAgICAgICAgdXJsOiBcImluZGV4Lmh0bWxcIixcbiAgICAgICAgZGF0YTogZGF0YVxuICAgIH0pLmRvbmUoZnVuY3Rpb24gKCkge1xuICAgICAgICAkKHRoaXMpLmZpbmQoXCJpbnB1dFwiKS52YWwoXCJcIik7XG4gICAgICAgICQoJy5tb2RhbC1jb250ZW50JykuY2hpbGRyZW4oKS5oaWRlKCk7XG4gICAgICAgIHNldFRpbWVvdXQgKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoJy50aGFua3MnKS5zaG93KCk7XG4gICAgICAgIH0sIDMwMCk7XG4gICAgICAgIHNldFRpbWVvdXQgKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoJy50aGFua3MnKS5mYWRlT3V0KCk7XG4gICAgICAgIH0sIDQwMDApO1xuICAgICAgICAkKFwiZm9ybVwiKS50cmlnZ2VyKFwicmVzZXRcIik7XG4gICAgICAgIHNldFRpbWVvdXQgKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICQoJy5tb2RhbC1jb250ZW50JykuY2hpbGRyZW4oKS5zaG93KCk7XG4gICAgICAgICAgICAkKCcudGhhbmtzJykuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcbiAgICAgICAgfSwgNTAwMCk7XG4gICAgfSk7XG4gICAgcmV0dXJuIGZhbHNlO1xufSk7XG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJCgnLmJ1dCcpLmNsaWNrKGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICQoJy5zZXJ2aWNlX2Rlc2MnKS5jaGlsZHJlbigpLmhpZGUoKTtcbiAgICAgICAgY29uc3QgbnVtID0gJCh0aGlzKS5hdHRyKCdkYXRhLW51bScpO1xuICAgICAgICAkKCcjcycrbnVtKS5zaG93KCk7XG4gICAgfSk7XG59KTtcblxuJChmdW5jdGlvbigpe1xuICAgIHZhciB1cmwgPSB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWU7XG4gICAgJCgnLm5hdi1saW5rJykuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBsaW5rID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XG4gICAgICAgIGlmKHVybCA9PSBsaW5rKSB7XG4gICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgfVxuICAgICAgICBpZih1cmwgPT09ICcvc2FsZS5odG1sJyB8fCB1cmwgPT09ICcvaW5zdGFsbC5odG1sJykge1xuICAgICAgICAgICAgJCgnLmFsbCcpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgfVxuICAgIH0pO1xufSk7XG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJCgnLnJlcGFpcicpLmNsaWNrKGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICAkKCcubW9yZScpLnNob3coKTtcbiAgICAgICAgICAgICQoJyNtc2cnKS5oaWRlKCk7XG4gICAgfSk7XG59KTtcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJCgnLnBpY3MnKS5jbGljayhmdW5jdGlvbihldmVudCkge1xuICAgICAgICAkKCcuc2VsbGVyJykudG9nZ2xlQ2xhc3MoJ3NlbGxlci1ncmlkJyk7XG4gICAgICAgICQoJy5zZWxsZXJfbGcnKS50b2dnbGVDbGFzcygnc2VsbGVyLWlubmVyLWdyaWQnKTtcbiAgICAgICAgJCgnLnNlbGxlci1pbm5lci1jb250ZW50JykudG9nZ2xlQ2xhc3MoJyBzZWxsZXItaW5uZXItZ3JpZCcpO1xuICAgICAgICAkKCcuc2VsbGVyLWRlc2MnKS5oaWRlKCk7XG4gICAgICAgICQoJy5zZWxsZXItYnV0dG9uJykuYWRkQ2xhc3MoJ3NlbGxlci1idXR0b24tZ3JpZCcpO1xuICAgICAgICAkKCcuYnV0dG9uLWRlc2MnKS5zaG93KCk7XG4gICAgICAgICQoJy5ncmlkJykucmVtb3ZlQ2xhc3MoJ2NvbC0xMicpLmFkZENsYXNzKCdjb2wtbGctMyBjb2wtbWQtNiBjb2wteHMtMTInKTtcbiAgICB9KTtcbiAgICAkKCcucm93cycpLmNsaWNrKGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICQoJy5ncmlkJykucmVtb3ZlQ2xhc3MoJ2NvbC1sZy0zIGNvbC1tZC02IGNvbC14cy0xMicpLmFkZENsYXNzKCdjb2wtMTInKTtcbiAgICAgICAgJCgnLnNlbGxlci1kZXNjJykuc2hvdygpO1xuICAgICAgICAkKCcuc2VsbGVyJykucmVtb3ZlQ2xhc3MoJ3NlbGxlci1ncmlkJyk7XG4gICAgICAgICQoJy5zZWxsZXJfbGcnKS5yZW1vdmVDbGFzcygnc2VsbGVyLWlubmVyLWdyaWQnKTtcbiAgICAgICAgJCgnLnNlbGxlci1pbm5lci1jb250ZW50JykucmVtb3ZlQ2xhc3MoJyBzZWxsZXItaW5uZXItZ3JpZCcpO1xuICAgICAgICAkKCcuc2VsbGVyLWJ1dHRvbicpLnJlbW92ZUNsYXNzKCdzZWxsZXItYnV0dG9uLWdyaWQnKTtcbiAgICAgICAgJCgnLmJ1dHRvbi1kZXNjJykuaGlkZSgpO1xuICAgIH0pO1xuXG4gICAgJCgnLmJ1dHRvbi1kZXNjJykuY2xpY2soZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGxldCBkZXNjID0gdGhpcy5zaWJsaW5ncygkKCcuc2VsbGVyLWRlc2MnKSk7XG4gICAgICAgIGRlc2MuY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJyk7XG4gICAgICAgIGRlc2MuYXBwZW5kKCQoJy5tb2RhbC1ib2R5LWRlc2MnKSlcblxuICAgIH0pXG59KTtcblxuXG5cblxuXG5cblxuXG5cbmNvbnN0IHNxbGl0ZTMgPSByZXF1aXJlKCdzcWxpdGUzJykudmVyYm9zZSgpO1xuXG5sZXQgZGIgPSBuZXcgc3FsaXRlMy5EYXRhYmFzZSgnLi9icmFuZHMuZGInLCAoZXJyKSA9PiB7XG4gICAgaWYgKGVycikge1xuICAgICAgICBjb25zb2xlLmVycm9yKGVyci5tZXNzYWdlKTtcbiAgICB9XG4gICAgY29uc29sZS5sb2coJ0Nvbm5lY3RlZCB0byB0aGUgYnJhbmRzIGRhdGFiYXNlLicpO1xufSk7XG5cbmxldCBicmFuZHMgPSBbXTtcbmxldCBzcWwgPSBgU0VMRUNUICogRlJPTSBCUkFORFMgV0hFUkUgaWQgPSA/YDtcbmxldCBpZCA9IDQ7XG5cbmRiLmdldChzcWwsIFtpZF0sIChlcnIsIHJvdykgPT4ge1xuICAgIGlmIChlcnIpIHtcbiAgICAgICAgcmV0dXJuIGNvbnNvbGUuZXJyb3IoZXJyLm1lc3NhZ2UpO1xuICAgIH1cbiAgICByZXR1cm4gcm93XG59KTtcblxuY29uc29sZS5sb2cocm93LmlkKTtcblxuLy8gZGIuYWxsKHNxbCwgW10sIChlcnIsIHJvd3MpID0+IHtcbi8vICAgICBpZiAoZXJyKSB7XG4vLyAgICAgICAgIHRocm93IGVycjtcbi8vICAgICB9IGVsc2Uge1xuLy8gICAgICAgICBzZXRWYWx1ZShyb3dzKTtcbi8vICAgICB9XG4vLyB9KTtcblxuLy8gZnVuY3Rpb24gc2V0VmFsdWUodmFsdWUpIHtcbi8vICAgICAgYnJhbmRzID0gdmFsdWU7XG4vLyAgICAgIGNvbnNvbGUubG9nKGJyYW5kcyk7XG4vLyB9XG5cblxuXG5cblxuXG4vLyBjb25zdCBnZXRSb3cgPSBmdW5jdGlvbihhKSB7XG4vLyAgICAgZGIuc2VyaWFsaXplKCgpID0+IHtcbi8vICAgICAgICAgZGIuZWFjaChgU0VMRUNUICogRlJPTSBicmFuZHMgV0hFUkUgaWQgPSBhYCwgKGVyciwgcm93KSA9PiB7XG4vLyAgICAgICAgICAgICBpZiAoZXJyKSB7XG4vLyAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIubWVzc2FnZSk7XG4vLyAgICAgICAgICAgICB9XG4vLyAgICAgICAgICAgICByZXR1cm4gcm93O1xuLy8gICAgICAgICAgICAgLy8gY29uc29sZS5sb2cocm93KTtcbi8vICAgICAgICAgfSk7XG4vLyAgICAgfSk7XG4vLyB9OyJdLCJmaWxlIjoibWFpbi5qcyJ9
